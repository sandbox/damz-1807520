<?php

$scenario_help[] = <<<EOF

CommerceExploreCatalog scenario
  Run parameters, comma separated:
    categories separated with colon
    products to visit per category
    add products to cart (1) or not (0)
  Example:
    "collection/carry:collection/geek-out,2,1"

EOF;

/**
 * Exploring product catalogs.
 */
class CommerceExploreCatalog extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $categories = isset($params[0]) ? $params[0] : '';
    $products_per_category = isset($params[1]) ? $params[1] : 0;
    $adding_to_cart = isset($params[2]) ? $params[2] : 0;

    $cats = !empty($categories) ? explode(':', $categories) : array();
    foreach ($cats as $cat) {
      $products = array();
      $category_url_prefix = '';
      if (is_numeric($cat)) {
        $category_url_prefix = 'taxonomy/term/';
      }
      foreach ($this->drupal->assertChildren($category_url_prefix . $cat, 'div.commerce-product-field a') as $product) {
        $products[] = $this->drupal->assertTag($product, 'href', '/');
      }
      shuffle($products);

      for ($index = 0; $index < $products_per_category; $index++) {
        if (isset($products[$index])) {
          if (!$adding_to_cart) {
            $this->drupal->visit($products[$index]);
          }
          else {
            $this->drupal->addToCart($products[$index]);
          }
        }
      }
    }
  }

}
