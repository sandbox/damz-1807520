<?php

$scenario_help[] = <<<EOF

DrupalLogin scenario
  Run parameters:
    None

EOF;

class DrupalLogin extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $this->drupal->login();
  }

}
