<?php

$scenario_help[] = <<<EOF

CommerceExploreSearch scenario
  Run parameters, comma separated:
    searching term
    products to visit per search
    add products to cart (1) or not (0)
  Example:
    "Tshirt,2,1"

EOF;

/**
 * Exploring products through search.
 */
class CommerceExploreSearch extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $search_term = isset($params[0]) ? htmlspecialchars($params[0]) : '';
    $products_per_category = isset($params[1]) ? $params[1] : 0;
    $adding_to_cart = isset($params[2]) ? $params[2] : 0;

    $products = array();
    foreach ($this->drupal->assertChildren("products?search_api_views_fulltext=$search_term", 'div.commerce-product-field a') as $product) {
      $products[] = $this->drupal->assertTag($product, 'href', '/');
    }
    shuffle($products);

    for ($index = 0; $index < $products_per_category; $index++) {
      if (isset($products[$index])) {
        if (!$adding_to_cart) {
          $this->drupal->visit($products[$index]);
        }
        else {
          $this->drupal->addToCart($products[$index]);
        }
      }
    }
  }

}
