<?php

$scenario_help[] = <<<EOF

DrupalInstall scenario
  Run parameters, comma separated:
    database name
    database username
    database passwrod
    site mail
  Example:
    "kickstart,sqluser,sqlpass,admin@example.com"

EOF;

class DrupalInstall extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $this->drupal->install($params[0], $params[1], $params[2], $params[3]);
  }
}
