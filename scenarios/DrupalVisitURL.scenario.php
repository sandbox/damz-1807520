<?php

$scenario_help[] = <<<EOF

DrupalVisitURL scenario
  Run parameters:
    url to visit
  Example:
    "content/about"

EOF;

class DrupalVisitURL extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $this->drupal->visit($params[0]);
  }

}
