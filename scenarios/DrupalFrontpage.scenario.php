<?php

$scenario_help[] = <<<EOF

DrupalFrontpage scenario
  Run parameters:
    None

EOF;

class DrupalFrontpage extends \Drupal\Scenario\DrupalScenario
{

  public function __construct($drupalSite) {
    parent::__construct($drupalSite);
  }

  public function run($params = array()) {
    $this->drupal->visit();
  }

}
