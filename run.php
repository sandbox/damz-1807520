#!/usr/bin/php
<?php
/**
 * @file
 * Scenario command-line runner.
 */

require_once 'vendor/autoload.php';
define('RUN_ROOT', dirname(__FILE__));

// Parse options.
$options = getopt('hn:u:s:n:c:v', array('user::', 'password::', 'params::'));
$options += array(
  'n' => 10,
  'c' => 1,
  'u' => '',
  's' => '',
  'user' => '',
  'password' => '',
  'params' => ''
);

// Display help is requirements are not met.
if (empty($options['u']) || empty($options['s']) || isset($options['h'])) {
  $script = basename(__FILE__);
  echo <<< EOF
Usage: $script [options]
  -n [number]: number of iterations (defaults to 10)
  -c [currency]: concurrency (defaults to 1)
  -u [url]: base URL to benchmark
  -s [scenario name]: name of the scenario
  -v : verbose output
  -vv : output scenario verbosity too
  --user="username": drupal username
  --password="password": drupal username password
  --params="parameters": scenario parameters

AVAILABLE SCENARIOS:

EOF;

  // Display help for each scenario.
  $scenario_help = array();
  foreach (glob(RUN_ROOT . "/scenarios/*.scenario.php") as $scenario) {
    include $scenario;
  }
  foreach ($scenario_help as $help) {
    echo $help;
  }
  exit(1);
}

// Initialize Drupal website.
$drupal = new \Drupal\Testing\DrupalTesting(
  $options['u'],
  $options['user'],
  $options['password']
);

// Set verbosity.
$verbose = FALSE;
if (isset($options['v'])) {
  $verbose = TRUE;
  if (is_array($options['v'])) {
    $drupal->beVerbose();
  }
}

// Set scenario params if provided.
$params = isset($options['params']) ? (strpos($options['params'], ',') ? explode(',', $options['params']) : array($options['params'])) : array('');

// Load scenario.
$scenario_file = RUN_ROOT . '/scenarios/' . $options['s'] . '.scenario.php';
if (!file_exists($scenario_file)) {
  echo "Scenario {$options['s']} does not exist.\n";
  exit(1);
}
include $scenario_file;

run_scenario($drupal, $options['s'], $options['n'], $options['c'], $params, $verbose);

/**
 * Multi-process runner.
 */
function run_scenario($website, $scenario, $iterations, $concurrency, $params = array(), $verbose = FALSE) {
  $finished = FALSE;
  $children = array();
  $success = array();
  $fail = array();

  while (!$finished || !empty($children)) {
    $spawned_children = FALSE;

    while (!$finished && count($children) < $concurrency) {
      try {
        $finished = $iterations-- < 1 ? TRUE : FALSE;
        if ($finished) break;

        $pid = pcntl_fork();
        if (!$pid) {
          // This is the child process.
          $action_object = new $scenario($website);
          $action_object->run($params);
          exit();
        }
        else {
          // Register our new child.
          $children[] = array('pid' => $pid, 'start' => microtime(TRUE));
          $spawned_children = TRUE;
          !$verbose || print("Running pid: $pid...\n");
        }
      }
      catch (Exception $e) {
        echo (string) $e . "\n";
      }
    }

    // Wait for children every 100ms.
    if (!$spawned_children) {
      usleep(100000);
    }

    // Check if some children finished.
    foreach ($children as $cid => $child) {
      if (pcntl_waitpid($child['pid'], $status, WUNTRACED | WNOHANG)) {
        // This particular child exited.
        if (pcntl_wifexited($status) && pcntl_wexitstatus($status) == 0) {
          $success[$child['pid']] = sprintf('%.3f', microtime(TRUE) - $child['start']);
          !$verbose || print("Finished pid: " . $child['pid'] . ".\n");
        }
        else {
          $fail[$child['pid']] = $child['pid'];
          !$verbose || print("Failed pid: " . $child['pid'] . ".\n");
        }
        unset($children[$cid]);
      }
    }
  }

  if (!$pid) {
    !$verbose || print("\n\n");
    echo "STATS\n-----\n";

    $total_time = 0.0;
    foreach ($success as $time) {
      $total_time += $time;
    }
    $avg_time = count($success) > 0 ? $total_time / count($success) : 0;
    $min_time = count($success) > 0 ? min($success) : 0;
    $max_time = count($success) > 0 ? max($success) : 0;

    echo "Finished:         " . count($success) . "\n";
    echo "Failed:           " . count($fail) . "\n";
    echo "Min/Max/Avg (s):  " . $min_time . "/" . $max_time . "/" . sprintf('%.3f', $avg_time) . "\n";
    echo "Total time (s):   " . sprintf('%.3f', $total_time) . "\n";
  }
}
