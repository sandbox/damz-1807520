<?php

namespace Drupal\Exception;

/**
 * Commerce Exception.
 */
class DrupalException extends \Exception
{
  /**
   * Initializes Drupal exception.
   */
  public function __construct($message, $code = 0, \Exception $previous = null) {
    parent::__construct($message);
  }
}
