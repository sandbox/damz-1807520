<?php

namespace Drupal\Scenario;

abstract class DrupalScenario
{
  protected $drupal;

  public function __construct($drupalInstance) {
    $this->drupal = $drupalInstance;
  }

  abstract protected function run($params = array());
}
