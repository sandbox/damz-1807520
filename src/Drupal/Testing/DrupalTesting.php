<?php

namespace Drupal\Testing;

/**
 * Drupal Testing class.
 */
class DrupalTesting
{
  /**
   * Mink session.
   */
  private $session;

  /**
   * Drupal base Url.
   */
  private $baseUrl;

  /**
   * Drupal username.
   */
  private $username;

  /**
   * Drupal password.
   */
  private $password;

  /**
   * Verbosity.
   */
  private $verbose;

  /**
   * Initializes DrupalTesting class.
   *
   * @param $baseUrl
   *   Url to the Drupal installation without trailing slash.
   * @param $username
   *   Drupal username.
   * @param $password
   *   Drupal password.
   * @param $timeout
   *   Request timeout in seconds.
   */
  public function __construct($baseUrl, $username = '', $password = '', $timeout = 60) {
    $this->session = new \Behat\Mink\Session(
      new \Behat\Mink\Driver\GoutteDriver(
        new \Goutte\Client(array('timeout' => $timeout))
      ),
      new \Behat\Mink\Selector\SelectorsHandler()
    );
    $this->baseUrl = $baseUrl . '/';
    $this->username = $username;
    $this->password = $password;
    $this->verbose = FALSE;
  }

  /**
   * Turns on verbosity.
   */
  public function beVerbose() {
    $this->verbose = TRUE;
  }

  /**
   * Visits page.
   *
   * @param $url
   *   Optional Url suffix in drupal l() form.
   */
  public function visit($url = '') {
    $this->session->visit($this->baseUrl . $url);
    !$this->verbose || print("Visited " . $this->baseUrl . $url . ".\n");
    // Fail on all status codes but 2XX.
    if (floor($this->session->getStatusCode() / 100) != 2) {
      $this->throwError('Unsuccessful, server returned ' . $this->session->getStatusCode() . ' code.');
    }
  }

  /**
   * Reloads the page.
   */
  public function reload() {
    $this->session->reload();
    !$this->verbose || print("Page reloaded.\n");
  }

  /**
   * Gets the page.
   *
   * @return
   *   An instance of Mink DocumentElement class.
   */
  public function getPage() {
    return $this->session->getPage();
  }

  /**
   * Gets HTML content of the page.
   *
   * @return
   *   Complete page HTML.
   */
  public function getPageContent() {
    !$this->verbose || print("Page content retrieved.\n");
    return $this->getPage()->getContent();
  }

  /**
   * Get current URL.
   *
   * @return
   *   URL.
   */
  public function getCurrentUrl() {
    $currentUrl = $this->session->getCurrentUrl();
    !$this->verbose || print("Current URL is $currentUrl.\n");
    return $currentUrl;
  }

  /**
   * Logs user in.
   */
  public function login() {
    if (empty($this->username) && empty($this->password)) {
      $this->throwError('Username and password must be provided for login.');
    }

    $this->visit('user');
    $this->assertExistance('input#edit-name')->setValue($this->username);
    $this->assertExistance('input#edit-pass')->setValue($this->password);
    $this->assertExistance('input#edit-submit')->press();

    if ($this->assertExistance('div.error')) {
      $this->throwError('Unable to login, please check username and password or if the account is blocked.');
    }
    !$this->verbose || print("Login performed.\n");
  }

  /**
   * Adds product to the cart.
   *
   * @param $nid
   *   Product display node identifier.
   */
  public function addToCart($prod) {
    $this->visit($prod);
    $this->assertExistance('form.commerce-add-to-cart input#edit-submit')->press();

    if (!$this->assertText('div.status', 'added to')) {
      $this->throwError('Failed to add ' . $prod . ' to the cart.');
    }
    !$this->verbose || print('Added ' . $prod . " to the cart.\n");
  }

  /**
   * Performs cart checkout.
   *
   * @param $level
   *   Checkout is made of 4 levels. With this paramter you can set
   *   which level you want to reach. Following levels are available:
   *     1 = Start checkout.
   *     2 = 1 and complete checkout information.
   *     3 = 1, 2 and select shipping method.
   *     4 = 1, 2, 3 and do the review completing the checkout.
   */
  public function checkout($level = 4) {
    $this->visit('cart');

    if ($this->assertText('div.cart-empty-page', 'is empty')) {
      $this->throwError('Can not checkout with the empty cart.');
    }

    $current_level = 0;
    while ($current_level++ < $level) {
      switch ($current_level) {

      case 1:
        // Start checkout from the cart.
        $this->assertExistance('input#edit-checkout')->press();
        !$this->verbose || print("Checkout started.\n");
        break;

      case 2:
        // Checkout form.
        $this->assertExistance('input#edit-account-login-mail')->setValue($this->randomString(10) . '@example.com');
        foreach (
          array(
            'edit-customer-profile-shipping-commerce-customer-address-und-0-name-line',
            'edit-customer-profile-shipping-commerce-customer-address-und-0-thoroughfare',
            'edit-customer-profile-shipping-commerce-customer-address-und-0-locality',
            'edit-customer-profile-shipping-commerce-customer-address-und-0-postal-code',
            'edit-customer-profile-billing-commerce-customer-address-und-0-name-line',
            'edit-customer-profile-billing-commerce-customer-address-und-0-thoroughfare',
            'edit-customer-profile-billing-commerce-customer-address-und-0-locality',
            'edit-customer-profile-billing-commerce-customer-address-und-0-postal-code'
          ) as $field) {
          $this->assertExistance('input#' . $field)->setValue($this->randomString(5));
        }
        foreach (
          array(
            'edit-customer-profile-shipping-commerce-customer-address-und-0-administrative-area',
            'edit-customer-profile-billing-commerce-customer-address-und-0-administrative-area'
          ) as $field) {
          $this->assertExistance('select#' . $field)->selectOption('MN');
        }
        $this->assertExistance('input#edit-continue')->press();

        if ($this->assertText('h2.element-invisible', 'Errors on form')) {
          $this->throwError('Checkout failed with errors on the checkout form.');
        }
        !$this->verbose || print("Checkout information submited.\n");
        break;

      case 3:
        // Select default shipping method.
        $this->assertExistance('input#edit-continue')->press();

        if ($this->assertText('h2.element-invisible', 'Errors on form')) {
          $this->throwError('Checkout failed with errors on the shipping form.');
        }
        !$this->verbose || print("Shipping method submited.\n");
        break;

      case 4:
        // Use default (example) payment method.
        $this->assertExistance('input#edit-commerce-payment-payment-details-name')->setValue($this->randomString(5));

        $this->assertExistance('input#edit-continue')->press();

        !$this->verbose || print("Payment method submited.\n");

        if ($this->assertText('h2.element-invisible', 'Errors on form')) {
          $this->throwError('Checkout failed with errors on the review form.');
        }

        // Assert checkout completition.
        $checkout_link = $this->assertExistance('div.checkout-completion-message a')->getAttribute('href');
        !$this->verbose || print("Checkout complete. View it here: $checkout_link.\n");
        break;
      }
    }
  }

  /**
   * Installs Commerce Kickstart V2.
   *
   * This method is tailored to Drupal Commerce Kickstart distribution.
   *
   * @param $db_name
   *   Database name.
   * @param $db_user
   *   Database username.
   * @param $db_pass
   *   Database password.
   * @param $site_mail
   *   Website mail.
   */
  public function install($db_name, $db_user, $db_pass, $site_mail) {
    $this->visit();
    $current_task = '.';

    while (!in_array($current_task, array('Finished', '', 'Verify requirements'))) {
      $current_task = trim($this->getCurrentTask($this->getInstallTasks()));
      !$this->verbose || print("Current task is $current_task.\n");
      if (strpos($this->getCurrentUrl(), '#') !== FALSE) {
        $this->visit();
      } else {
        $this->reload();
      }

      if ($this->assertExistance('input#edit-driver-mysql', FALSE)) {
        $this->assertExistance('input#edit-mysql-database')->setValue($db_name);
        $this->assertExistance('input#edit-mysql-username')->setValue($db_user);
        $this->assertExistance('input#edit-mysql-password')->setValue($db_pass);
      }

      if ($this->assertExistance('input#edit-site-mail', FALSE)) {
        $this->assertExistance('input#edit-site-mail')->setValue($site_mail);
      }

      if ($this->assertExistance('div.progress', FALSE)) {
        while ($this->assertExistance('div.progress', FALSE) && !$this->assertText('div.percentage', '100%')) {
          $this->reload();
          // Pause spares poor CPU.
          sleep(1);
          if ($this->verbose) {
            $message = $this->assertExistance('div.message', FALSE);
            if ($message) {
              print(str_replace('.', '. ', $message->getText()) . "\n");
            } else {
              print("Not found element div.message.\n");
            }
          }
        }

        if ($current_task == 'Install profile') {
          $this->visit('install.php?locale=en');
        }
        else {
          $this->visit();
        }
      }
      else {
        if ($this->assertExistance('input#edit-submit', FALSE)) {
          $this->assertExistance('input#edit-submit')->press();
        }
        else if ($this->assertExistance('input#edit-save', FALSE)) {
          $this->assertExistance('input#edit-save')->press();
        }
      }

      sleep (1);
      // Overlay fails, so we must remove it.
      if (strpos($this->getCurrentUrl(), '#') !== FALSE) {
        $this->visit();
      }
      else {
        $this->reload();
      }
    }

    switch($current_task) {
    case 'Finished':
      // Installation finished.
      $this->visit();
      break;

    case 'Verify requirements':
      $this->throwError('Unmet requirements for Drupal installation.');
      break;

    case '':
      break;

    default:
      $this->throwError("Uknown installation task $current_task.");
    }

    // Assert we got to the front page.
    if (!$this->assertText('h1.site-name', 'Commerce Kickstart')) {
      $this->throwError('Installation failed, no front page can be found.');
    }
    !$this->verbose || print("Installation finished.\n");
  }

  /**
   * Asserts element exsistance.
   *
   * @param $selector
   *   CSS selector for the element.
   * @param $force_existance
   *   Throw exception if element is not found.
   * @return
   *   Instance of NodeElement class.
   */
  public function assertExistance($selector, $force_existance = TRUE) {
    $element = $this->getPage()->find('css', $selector);
    if (!$element && $force_existance) {
      $this->throwError("Element $selector does not exist.");
    }

    !$this->verbose || print(($element ? 'Found' : 'Not found') . ' element ' . $selector . ".\n");

    return $element;
  }

  /**
   * Asserts multiple elements exsistance.
   *
   * @param $selector
   *   CSS selector for the elements.
   * @param $force_existance
   *   Throw exception if not one element is not found.
   * @return
   *   Array with NodeElement objects.
   */
  public function assertExistanceAll($selector, $force_existance = TRUE) {
    $elements = $this->getPage()->findAll('css', $selector);
    if (!$elements && $force_existance) {
      $this->throwError("Elements $selector do not exist.");
    }

    !$this->verbose || print(($elements ? 'Found ' . count($elements) : 'Not found') . ' elements ' . $selector . ".\n");

    return $elements;
  }

  /**
   * Asserts element text.
   *
   * @param $selector
   *   CSS selector for the element, or NodeElement object.
   * @param $text
   *   Text to assert in the element.
   * @return
   *   TRUE if text is found, FALSE otherwise.
   */
  public function assertText($selector, $text) {
    if (!is_object($selector)) {
      $element = $this->assertExistance($selector, FALSE);
    }

    if ($element && strpos($element->getText(), $text) !== FALSE) {
      !$this->verbose || print('Found "' . $text . '" in ' . $selector . ".\n");
      return TRUE;
    }

    !$this->verbose || print('Did not found "' . $text . '" in ' . $selector . ".\n");
    return FALSE;
  }

  /**
   * Asserts element tag.
   *
   * This method will make result URLs clean (ie. without ?q= part).
   *
   * @param $element
   *   NodeElement object.
   * @param $tag
   *   Tag to search within element.
   * @param $leftTrim
   *   If not empty, remove this char from the left. Usable with the links.
   * @return
   *   Value of the tag.
   */
  public function assertTag($element, $tag, $leftTrim = '') {
    if (get_class($element) != 'Behat\Mink\Element\NodeElement') {
      $this->throwError("Invalid element in assertTag.");
    }
    $result = $element->getAttribute($tag);

    if ($tag == 'href') {
      // Force clean URL links.
      if (strpos($result, '?') !== FALSE) {
        $query_parts = explode('=', $result);
        $result = $query_parts[1];
      }
    }

    if ($leftTrim) {
      $result = ltrim($result, $leftTrim);
    }

    !$this->verbose || print('Found "' . $result . '" in ' . $tag . ".\n");
    return $result;
  }

  /**
   * Asserts element children within page.
   *
   * @param $url
   *   URL to search children from.
   * @param $selector
   *   CSS selector for the children elements.
   * @return
   *   Found children elements as an array.
   */
  public function assertChildren($url, $selector) {
    $this->visit($url);
    $children = array();
    foreach ($this->assertExistanceAll($selector) as $child) {
      $children[] = $child;
    }
    !$this->verbose || print('Found ' . count($children) . " of $selector in $url.\n");
    return $children;
  }

  /**
   * Gets Drupal Commerce installation tasks.
   *
   * @return
   *   Array with all the tasks and its statuses.
   */
  protected function getInstallTasks() {
    $tasks = array();
    $task_list = $this->assertExistanceAll('.task-list li', FALSE);

    foreach ($task_list as $task_el) {
      $tasks[trim(preg_replace('/\s*\([^)]*\)/', '', $task_el->getText()))] = $task_el->getAttribute('class') ? $task_el->getAttribute('class') : 'todo';
    }

    return $tasks;
  }

  /**
   * Gets current installation task.
   *
   * @param $tasks
   *   Array with the tasks.
   * @return
   *   Name of the current installation task.
   *
   * @see this::getInstallTasks()
   */
  protected function getCurrentTask($tasks) {
    foreach ($tasks as $name => $status) {
      if ($status == 'active') {
        return $name;
      }
    }
  }

  /**
   * Throws CommerceTesting exception.
   *
   * @param $message
   *   Message for the exception.
   */
  protected function throwError($message) {
    throw new \Drupal\Exception\DrupalException($message);
  }

  /**
   * Makes a random string.
   *
   * @param $length
   *   The length of the random string.
   * @param $numeralOnly
   *   If TRUE, return only random numbers.
   * @return
   *   Random string.
   */
  private function randomString($length = 10, $numeralOnly = FALSE) {
    $base = $numeralOnly ? '123456789' : 'abcdefghjkmnpqrstwxyz';
    $max = strlen($base) - 1;
    $string = '';
    mt_srand(microtime(TRUE) * 1000000);
    while (strlen($string) < $length) {
      $string .= $base{mt_rand(0, $max)};
    }
    return $string;
  }
}
